/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : CompoundEvaluation
 * Author      : Damien Nguyen
 * Date        : Friday, June 26 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.business;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import java.util.ArrayList;

public abstract class EvaluationTab<T extends Evaluation> extends Evaluation implements Evaluable<T>, Observer {

    protected FloatProperty   average    = new SimpleFloatProperty();
    protected ListProperty<T> components = new SimpleListProperty<>(FXCollections.observableList(new ArrayList<>()));

    public EvaluationTab() {
        super();

        this.components.addListener((ListChangeListener<? super T>) change -> this.average.set(average()));
        this.average.addListener((v, o, n) -> alert());
    }

    public EvaluationTab(float coefficient, String label) {
        this();

        setCoefficient(coefficient);
        setLabel(label);
    }

    protected static float compoundAverage(ListProperty<? extends EvaluationTab<?>> collection) {
        float sum = 0, count = 0;

        for (var tab : collection) {
            if (tab.hasEvaluations()) {
                sum += tab.average() * tab.getCoefficient();
                count += tab.getCoefficient();
            }
        }

        return sum / (count == 0 ? 1 : count);
    }

    public ObservableList<T> getComponents() {
        return components.get();
    }

    public void setComponents(ObservableList<T> components) {
        this.components.set(components);
    }

    public ListProperty<T> componentsProperty() {
        return components;
    }

    public float getAverage() {
        return average.get();
    }

    public void setAverage(float average) {
        this.average.set(average);
    }

    public FloatProperty averageProperty() {
        return average;
    }

    public abstract float average();

    public abstract boolean hasEvaluations();

    public void add(T element) {
        components.add(element);
        components.sort(T::compareTo);
        element.attach(this);
        alert();
    }

    public void remove(T element) {
        components.remove(element);
        element.detach(this);
        alert();
    }

    @Override
    public void update() {
        average.set(average());
    }

}
