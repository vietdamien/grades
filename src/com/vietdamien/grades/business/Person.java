/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Person
 * Author      : Damien Nguyen
 * Date        : Thursday, June 25 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.business;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public abstract class Person {

    protected StringProperty lastName;
    protected StringProperty firstName;
    protected StringProperty email;

    public Person() {
        this.lastName  = new SimpleStringProperty();
        this.firstName = new SimpleStringProperty();
        this.email     = new SimpleStringProperty();
    }

    public Person(String lastName, String firstName, String email) {
        this();

        setLastName(lastName);
        setFirstName(firstName);
        setEmail(email);
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public StringProperty emailProperty() {
        return email;
    }

    public boolean isEmpty() {
        return (getLastName() == null && getFirstName() == null && getEmail() == null)
               || (getLastName().trim().isEmpty() && getFirstName().trim().isEmpty() && getEmail().trim().isEmpty());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Person person = (Person) o;

        return getLastName().equals(person.getLastName())
               && getFirstName().equals(person.getFirstName())
               && getEmail().equals(person.getEmail());
    }

    @Override
    public int hashCode() {
        int result = getLastName().hashCode();
        result = 31 * result + getFirstName().hashCode();
        result = 31 * result + getEmail().hashCode();
        return result;
    }

}
