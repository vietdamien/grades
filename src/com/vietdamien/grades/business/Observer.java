package com.vietdamien.grades.business;
/*
 *********************************************************************************************************************
 * Project name: Grades
 * File name   : Observer
 * Author      : Damien Nguyen
 * Date        : Monday, June 29 2020
 * ********************************************************************************************************************/

public interface Observer {

    void update();

}
