/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Subject
 * Author      : Damien Nguyen
 * Date        : Friday, June 26 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.business;

public class Subject extends EvaluationTab<Grade> {

    public Subject() {
        super();
    }

    public Subject(float coefficient, String label) {
        super(coefficient, label);
    }

    @Override
    public float average() {
        float sum = 0, count = 0;

        for (var grade : components) {
            sum += grade.getValue() * grade.getCoefficient();
            count += grade.getCoefficient();
        }

        return sum / (count == 0 ? 1 : count);
    }

    @Override
    public boolean hasEvaluations() {
        return !components.isEmpty();
    }

}
