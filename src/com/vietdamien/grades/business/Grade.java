/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Grade
 * Author      : Damien Nguyen
 * Date        : Thursday, June 25 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.business;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;

public class Grade extends Evaluation {

    protected FloatProperty value = new SimpleFloatProperty();

    public Grade() {
        super();

        this.value.addListener((v, o, n) -> alert());
    }

    public Grade(float coefficient, float value, String label) {
        this();

        setCoefficient(coefficient);
        setValue(value);
        setLabel(label);
    }

    public float getValue() {
        return value.get();
    }

    public void setValue(float value) {
        this.value.set(value);
        alert();
    }

    public FloatProperty valueProperty() {
        return value;
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty() && getValue() == 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Grade grade = (Grade) o;

        return Math.abs(grade.getValue() - getValue()) < EPSILON;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getValue() != +0.0f ? Float.floatToIntBits(getValue()) : 0);
        return result;
    }

}
