/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Student
 * Author      : Damien Nguyen
 * Date        : Friday, June 26 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.business;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import java.util.ArrayList;

public class Student extends Person implements Evaluable<Module>, Observer {

    protected ListProperty<Module> modules = new SimpleListProperty<>(FXCollections.observableList(new ArrayList<>()));
    protected FloatProperty        average = new SimpleFloatProperty();

    public Student() {
        super();

        this.modules.addListener((ListChangeListener<? super Module>) change -> update());
    }

    public Student(String lastName, String firstName, String email) {
        this();

        setLastName(lastName);
        setFirstName(firstName);
        setEmail(email);
    }

    public ObservableList<Module> getModules() {
        return modules.get();
    }

    public void setModules(ObservableList<Module> modules) {
        this.modules.set(modules);
    }

    public ListProperty<Module> modulesProperty() {
        return modules;
    }

    public float getAverage() {
        return average.get();
    }

    public void setAverage(float average) {
        this.average.set(average);
    }

    public FloatProperty averageProperty() {
        return average;
    }

    @Override
    public void add(Module module) {
        if (!modules.contains(module)) {
            modules.add(module);
            module.attach(this);
            modules.sort(Module::compareTo);
        }
    }

    @Override
    public void remove(Module module) {
        modules.remove(module);
        module.detach(this);
    }

    public float average() {
        return EvaluationTab.compoundAverage(modules);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Student student = (Student) o;

        return modules.equals(student.modules);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + modules.hashCode();
        return result;
    }

    @Override
    public void update() {
        average.set(average());
    }

}
