/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Module
 * Author      : Damien Nguyen
 * Date        : Friday, June 26 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.business;

public class Module extends EvaluationTab<Subject> {

    public Module() {
        super();
    }

    public Module(float coefficient, String label) {
        super(coefficient, label);
    }

    @Override
    public float average() {
        return compoundAverage(components);
    }

    @Override
    public boolean hasEvaluations() {
        return components.stream().anyMatch(Subject::hasEvaluations);
    }

}
