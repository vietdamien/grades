package com.vietdamien.grades.business;/*
*********************************************************************************************************************
 * Project name: Grades
 * File name   : Evaluable
 * Author      : Damien Nguyen
 * Date        : Tuesday, July 21 2020
 * ********************************************************************************************************************/

public interface Evaluable<T extends Evaluation> {

    void add(T evaluation);

    void remove(T evaluation);

}
