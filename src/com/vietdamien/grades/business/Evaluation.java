/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Evaluation
 * Author      : Damien Nguyen
 * Date        : Friday, June 26 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.business;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.List;

public abstract class Evaluation implements Comparable<Evaluation>, Observable {

    public static final double EPSILON = 1e-2;

    protected List<Observer> observers;
    protected StringProperty label       = new SimpleStringProperty();
    protected FloatProperty  coefficient = new SimpleFloatProperty();

    public Evaluation() {
        this.observers = new ArrayList<>();

        this.label.addListener((v, o, n) -> alert());
        this.coefficient.addListener((v, o, n) -> alert());
    }

    public Evaluation(float coefficient, String label) {
        this();

        setCoefficient(coefficient);
        setLabel(label);
    }

    public List<Observer> getObservers() {
        return observers;
    }

    public void setObservers(List<Observer> observers) {
        this.observers = observers;
    }

    public String getLabel() {
        return label.get();
    }

    public void setLabel(String label) {
        this.label.set(label);
    }

    public StringProperty labelProperty() {
        return label;
    }

    public float getCoefficient() {
        return coefficient.get();
    }

    public void setCoefficient(float coefficient) {
        this.coefficient.set(coefficient);
    }

    public FloatProperty coefficientProperty() {
        return coefficient;
    }

    public boolean isEmpty() {
        return getLabel().trim().isEmpty() && getCoefficient() == 0;
    }

    @Override
    public int compareTo(Evaluation o) {
        return getLabel().compareTo(o.getLabel());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Evaluation that = (Evaluation) o;
        return getLabel().equals(that.getLabel())
               && Math.abs(that.getCoefficient() - getCoefficient()) < EPSILON;
    }

    @Override
    public int hashCode() {
        int result = label.hashCode();
        result = 31 * result + (getCoefficient() != 0.0f ? Float.floatToIntBits(getCoefficient()) : 0);
        return result;
    }

    @Override
    public void attach(Observer o) {
        observers.add(o);
    }

    @Override
    public void detach(Observer o) {
        observers.remove(o);
    }

    @Override
    public void alert() {
        for (var observer : observers) {
            observer.update();
            if (observer instanceof Observable observable) {
                observable.alert();
            }
        }
    }

}
