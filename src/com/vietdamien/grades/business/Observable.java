package com.vietdamien.grades.business;
/*
 *********************************************************************************************************************
 * Project name: Grades
 * File name   : Observable
 * Author      : Damien Nguyen
 * Date        : Monday, June 29 2020
 * ********************************************************************************************************************/

public interface Observable {

    void attach(Observer o);

    void detach(Observer o);

    void alert();

}
