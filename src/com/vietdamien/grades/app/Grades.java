/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Grades
 * Author      : Damien Nguyen
 * Date        : Thursday, June 25 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.app;

import com.vietdamien.grades.controllers.Home;
import com.vietdamien.grades.data.DataManager;
import com.vietdamien.grades.util.view.WindowUtil;
import javafx.application.Application;
import javafx.stage.Stage;

public class Grades extends Application {

    private DataManager manager;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        WindowUtil.create(new Home(this.manager = new DataManager()), primaryStage, true);
    }

    @Override
    public void stop() throws Exception {
        super.stop();

        if (!manager.getStudent().isEmpty()) {
            manager.save();
        }
    }

}
