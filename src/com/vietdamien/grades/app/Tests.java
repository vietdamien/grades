/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Tests
 * Author      : Damien Nguyen
 * Date        : Thursday, June 25 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.app;

import com.vietdamien.grades.business.Evaluation;
import com.vietdamien.grades.data.persistence.stub.StubDataLoader;
import com.vietdamien.grades.util.debug.Validator;

public class Tests {

    private final Validator validator;

    private Tests() {
        this.validator = new Validator();
    }

    public static void main(String[] args) {
        Tests t = new Tests();

        t.testStudent();
    }

    private void testStudent() {
        var s = new StubDataLoader().load();

        this.validator.validate(Math.abs(s.average() - 16) < Evaluation.EPSILON);
        var it = s.getModules().iterator();
        for (int i = 0; i < 3; ++i) {
            it.next();
        }
        this.validator.validate(Math.abs(it.next().average() - 16) < Evaluation.EPSILON);

        this.validator.endTests("Student");
    }

}
