/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Factory
 * Author      : Damien Nguyen
 * Date        : Tuesday, July 21 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.util.logic;

public abstract class Factory<T> {

    public abstract T create();

}
