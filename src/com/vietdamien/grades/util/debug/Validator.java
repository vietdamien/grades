/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Validator
 * Author      : Damien Nguyen
 * Date        : Thursday, June 25 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.util.debug;

public class Validator {

    public void endTests(String name) {
        System.out.println(name + " tests passed successfully.");
    }

    public void validate(boolean predicate) {
        if (!predicate) {
            throw new RuntimeException("TEST FAILED!");
        }
    }

}
