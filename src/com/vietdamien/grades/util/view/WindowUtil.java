/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : WindowUtil
 * Author      : Damien Nguyen
 * Date        : Thursday, June 25 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.util.view;

import com.vietdamien.grades.controllers.Controller;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;

public class WindowUtil {

    /**
     * The default height of a Window.
     */
    public static final int    HEIGHT = 720;
    /**
     * The default width of a Window.
     */
    public static final int    WIDTH  = 1280;
    /**
     * The default title of a Window.
     */
    public static final String TITLE  = "Grades";

    private WindowUtil() {
    }

    public static void close(Window window) {
        ((Stage) window).close();
    }

    public static void create(Controller controller) {
        create(controller, false);
    }

    public static void create(Controller controller, boolean maximized) {
        create(controller, new Stage(), maximized);
    }

    public static void create(Controller controller, Stage stage) {
        create(controller, stage, false);
    }

    public static void create(Controller controller, Stage stage, boolean maximized) {
        display(buildScene(controller), stage, maximized);
    }

    public static void create(Controller controller, Window owner) {
        create(controller, owner, false);
    }

    public static void create(Controller controller, Window owner, boolean maximized) {
        display(buildScene(controller), new Stage(), owner, maximized);
    }

    private static Scene buildScene(Controller controller) {
        var loader = new FXMLLoader(WindowUtil.class.getResource(Paths.getView(controller)), I18n.getBundle());
        loader.setController(controller);

        try {
            var scene = new Scene(loader.load(), WIDTH, HEIGHT);
            scene.getStylesheets().add(Paths.STYLESHEET);
            return scene;
        } catch (IOException e) {
            throw new RuntimeException("Path not found.");
        }
    }

    private static void display(Scene scene, Stage stage, boolean maximized) {
        stage.setMaximized(maximized);
        stage.setTitle(I18n.getString(TITLE));
        stage.setScene(scene);
        stage.show();
    }

    private static void display(Scene scene, Stage stage, Window owner, boolean maximized) {
        stage.initOwner(owner);
        display(scene, stage, maximized);
    }

}
