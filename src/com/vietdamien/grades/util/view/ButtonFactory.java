/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : ButtonsFactory
 * Author      : Damien Nguyen
 * Date        : Tuesday, July 21 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.util.view;

import com.vietdamien.grades.util.logic.Factory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

import java.util.List;

public class ButtonFactory extends Factory<Button> {

    protected String                    key;
    protected List<String>              styleClasses;
    protected EventHandler<ActionEvent> handler;

    public ButtonFactory(String key, List<String> styleClasses, EventHandler<ActionEvent> handler) {
        this.key          = key;
        this.styleClasses = styleClasses;
        this.handler      = handler;
    }

    @Override
    public Button create() {
        var button = new Button(I18n.getString(key));

        button.getStyleClass().addAll(styleClasses);
        button.setOnAction(handler);

        return button;
    }

}
