/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Paths
 * Author      : Damien Nguyen
 * Date        : Thursday, June 25 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.util.view;

public class Paths {

    /**
     * The resource bundle path.
     */
    public static final String I18N = "i18n.messages";

    /**
     * The stylesheet path.
     */
    public static final String STYLESHEET = "/css/style.css";

    /**
     * The FXML extension.
     */
    public static final String VIEW_EXTENSION = ".fxml";

    /**
     * The view root directory.
     */
    public static final String VIEW_ROOT = "/view/";

    private Paths() {
    }

    /**
     * Builds the complete path to the view associated to the given controller.
     *
     * @param controller the given controller
     * @return the complete path to the view associated to the given controller
     */
    public static String getView(Object controller) {
        return VIEW_ROOT + controller.getClass().getSimpleName() + VIEW_EXTENSION;
    }

}
