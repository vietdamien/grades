/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : I18n
 * Author      : Damien Nguyen
 * Date        : Thursday, June 25 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.util.view;

import java.util.ResourceBundle;

public class I18n {

    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle(Paths.I18N);

    private I18n() {
    }

    /**
     * Gets the ResourceBundle that defines the language of this application.
     *
     * @return the ResourceBundle
     */
    public static ResourceBundle getBundle() {
        return BUNDLE;
    }

    /**
     * Gets the string that corresponds to the given key.
     *
     * @param key the given key
     * @return the string that corresponds to the given key
     */
    public static String getString(String key) {
        return BUNDLE.getString(key);
    }

}
