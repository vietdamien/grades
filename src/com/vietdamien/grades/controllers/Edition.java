/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Edition
 * Author      : Damien Nguyen
 * Date        : Tuesday, July 21 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.controllers;

import com.vietdamien.grades.data.DataManager;
import com.vietdamien.grades.util.view.ButtonFactory;
import com.vietdamien.grades.util.view.WindowUtil;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import javafx.stage.Window;

import java.util.List;

public abstract class Edition extends Controller {

    protected Window window;
    @FXML
    private   VBox   buttons;

    public Edition(DataManager manager) {
        super(manager);
    }

    protected void buildRemoveButton(EventHandler<ActionEvent> handler) {
        var button = new ButtonFactory("Remove", List.of("infinite-width", "red-bg", "text-white"), handler);
        var nodes  = buttons.getChildren();

        nodes.add(nodes.size() - 1, button.create());
    }

    @FXML
    protected void cancel() {
        close();
    }

    @FXML
    protected void submit() {
        close();
    }

    protected void close() {
        WindowUtil.close(getWindow());
    }

    protected Window getWindow() {
        if (window == null) {
            window = buttons.getScene().getWindow();
        }

        return window;
    }

}
