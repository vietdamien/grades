/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : GradeEdition
 * Author      : Damien Nguyen
 * Date        : Wednesday, July 01 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.controllers;

import com.vietdamien.grades.business.Grade;
import com.vietdamien.grades.business.Subject;
import com.vietdamien.grades.data.DataManager;
import com.vietdamien.grades.util.view.WindowUtil;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.util.converter.NumberStringConverter;

public class GradeEdition extends Edition {

    protected Grade   grade;
    protected Grade   oldGrade;
    protected Subject subject;

    @FXML
    private TextField label;
    @FXML
    private TextField value;
    @FXML
    private TextField coefficient;

    @FXML
    private VBox buttons;

    public GradeEdition(DataManager manager, Subject subject) {
        this(manager, new Grade(0, 0, ""), subject);
    }

    public GradeEdition(DataManager manager, Grade grade, Subject subject) {
        super(manager);

        if (!grade.isEmpty()) {
            this.oldGrade = new Grade(grade.getCoefficient(), grade.getValue(), grade.getLabel());
        }

        this.grade   = grade;
        this.subject = subject;
    }

    @FXML
    private void initialize() {
        var converter = new NumberStringConverter();

        label.textProperty().bindBidirectional(grade.labelProperty());
        Bindings.bindBidirectional(value.textProperty(), grade.valueProperty(), converter);
        Bindings.bindBidirectional(coefficient.textProperty(), grade.coefficientProperty(), converter);

        if (oldGrade != null) {
            buildRemoveButton(event -> {
                subject.remove(grade);
                WindowUtil.close(buttons.getScene().getWindow());
            });
        }
    }

    @Override
    protected void cancel() {
        if (oldGrade != null) {
            grade.setCoefficient(oldGrade.getCoefficient());
            grade.setValue(oldGrade.getValue());
            grade.setLabel(oldGrade.getLabel());
        }

        super.cancel();
    }

    @Override
    protected void submit() {
        if (!grade.isEmpty() && oldGrade == null) {
            subject.add(grade);
        }

        super.submit();
    }

    @Override
    protected void close() {
        label.textProperty().unbindBidirectional(grade.labelProperty());
        value.textProperty().unbindBidirectional(grade.valueProperty());
        coefficient.textProperty().unbindBidirectional(grade.coefficientProperty());

        super.close();
    }

}
