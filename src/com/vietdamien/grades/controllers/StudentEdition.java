/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : StudentEdition
 * Author      : Damien Nguyen
 * Date        : Wednesday, July 22 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.controllers;

import com.vietdamien.grades.business.Student;
import com.vietdamien.grades.data.DataManager;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class StudentEdition extends Edition {

    private final Student oldStudent;

    @FXML
    private TextField lastName;
    @FXML
    private TextField firstName;
    @FXML
    private TextField email;

    public StudentEdition(DataManager manager) {
        super(manager);

        var student = manager.getStudent();
        this.oldStudent = !student.isEmpty()
                          ? new Student(student.getLastName(), student.getFirstName(), student.getEmail())
                          : null;
    }

    @FXML
    private void initialize() {
        var student = manager.getStudent();

        lastName.textProperty().bindBidirectional(student.lastNameProperty());
        firstName.textProperty().bindBidirectional(student.firstNameProperty());
        email.textProperty().bindBidirectional(student.emailProperty());
    }

    @Override
    protected void cancel() {
        var student = manager.getStudent();

        if (oldStudent != null) {
            student.setLastName(oldStudent.getLastName());
            student.setFirstName(oldStudent.getFirstName());
            student.setEmail(oldStudent.getEmail());
        }

        if (oldStudent != null && !oldStudent.isEmpty()) {
            super.cancel();
        }
    }

    @Override
    protected void submit() {
        if (!manager.getStudent().isEmpty()) {
            super.submit();
        }
    }

    @Override
    protected void close() {
        var student = manager.getStudent();

        lastName.textProperty().unbindBidirectional(student.lastNameProperty());
        firstName.textProperty().unbindBidirectional(student.firstNameProperty());
        email.textProperty().unbindBidirectional(student.emailProperty());

        super.close();
    }

}
