package com.vietdamien.grades.controllers;/*
 *********************************************************************************************************************
 * Project name: Grades
 * File name   : DoubleSidedPane
 * Author      : Damien Nguyen
 * Date        : Sunday, July 19 2020
 * ********************************************************************************************************************/

public interface DoubleSidedPane {

    void flip(Object o);

}
