/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Controller
 * Author      : Damien Nguyen
 * Date        : Wednesday, July 01 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.controllers;

import com.vietdamien.grades.data.DataManager;

public abstract class Controller {

    protected DataManager manager;

    public Controller(DataManager manager) {
        this.manager = manager;
    }

}
