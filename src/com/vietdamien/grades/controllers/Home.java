/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Home
 * Author      : Damien Nguyen
 * Date        : Thursday, June 25 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.controllers;

import com.vietdamien.grades.business.Grade;
import com.vietdamien.grades.business.Module;
import com.vietdamien.grades.business.Subject;
import com.vietdamien.grades.controllers.cells.GradeListCell;
import com.vietdamien.grades.controllers.cells.ModuleListCell;
import com.vietdamien.grades.data.DataManager;
import com.vietdamien.grades.util.view.I18n;
import com.vietdamien.grades.util.view.WindowUtil;
import javafx.beans.binding.Bindings;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

public class Home extends Edition implements DoubleSidedPane {

    //region Header
    @FXML
    private Label lastName;
    @FXML
    private Label firstName;
    @FXML
    private Label email;
    //endregion

    //region Content
    @FXML
    private StackPane content;

    //region Main tab
    @FXML
    private BorderPane        main;
    @FXML
    private Label             average;
    @FXML
    private ListView<Module>  modules;
    @FXML
    private ListView<Subject> subjects;
    @FXML
    private Button            moduleAdder;
    //endregion

    //region Subject detail
    @FXML
    private BorderPane      detail;
    @FXML
    private BorderPane      detailHeader;
    @FXML
    private Label           coefficient;
    @FXML
    private Label           subject;
    @FXML
    private Label           subjectAverage;
    @FXML
    private ListView<Grade> grades;
    @FXML
    private Button          gradeAdder;
    //endregion
    //endregion

    private EventHandler<MouseEvent> gradeHandler;

    public Home(DataManager manager) {
        super(manager);
    }

    @FXML
    private void initialize() {
        var student = manager.getStudent();

        lastName.textProperty().bind(student.lastNameProperty());
        firstName.textProperty().bind(student.firstNameProperty());
        email.textProperty().bind(student.emailProperty());
        average.textProperty().bind(Bindings.format("%05.2f", student.averageProperty()));
        grades.setCellFactory(gradeListView -> new GradeListCell(manager));

        modules.setCellFactory(moduleListView -> new ModuleListCell(manager, this));
        modules.itemsProperty().bind(manager.getStudent().modulesProperty());
        modules.prefHeightProperty().bind(Bindings.size(modules.itemsProperty().get()).multiply(37.5));
        modules.setContextMenu(buildModuleContextMenu());
    }

    @FXML
    private void createModule() {
        WindowUtil.create(new EvaluationTabEdition<>(manager, manager.getStudent(), Module.class), getWindow());
    }

    @FXML
    private void displayModules() {
        subjectAverage.textProperty().unbind();
        coefficient.textProperty().unbind();
        subject.textProperty().unbind();
        grades.itemsProperty().unbind();

        if (gradeHandler != null) {
            grades.removeEventFilter(MouseEvent.MOUSE_CLICKED, gradeHandler);
        }

        main.toFront();
    }

    @FXML
    private void editStudent() {
        WindowUtil.create(new StudentEdition(manager), getWindow());
    }

    @Override
    public void flip(Object o) {
        var selected = (Subject) o;

        subjectAverage.textProperty().bind(Bindings.format("%05.2f", selected.averageProperty()));
        coefficient.textProperty().bind(Bindings.format("%05.2f", selected.coefficientProperty()));
        subject.textProperty().bind(selected.labelProperty());
        grades.itemsProperty().bind(selected.componentsProperty());

        grades.addEventFilter(MouseEvent.MOUSE_CLICKED, gradeHandler = event -> {
            var grade = grades.getSelectionModel().getSelectedItem();
            if (grade != null && event.getButton() == MouseButton.PRIMARY) {
                WindowUtil.create(new GradeEdition(manager, grade, selected), getWindow());
            }
        });
        gradeAdder.setOnAction(event -> {
            WindowUtil.create(new GradeEdition(manager, selected), getWindow());
        });

        detail.toFront();
    }

    private ContextMenu buildModuleContextMenu() {
        var m1      = new MenuItem(I18n.getString("Edit_module"));
        var m2      = new MenuItem(I18n.getString("Remove_module"));
        var m3      = new MenuItem(I18n.getString("Add_subject"));
        var menu    = new ContextMenu(m1, m2, m3);
        var student = manager.getStudent();

        m1.setOnAction(event -> WindowUtil.create(
                new EvaluationTabEdition<>(manager, student, modules.getSelectionModel().getSelectedItem()), getWindow()
        ));
        m2.setOnAction(event -> student.remove(modules.getSelectionModel().getSelectedItem()));
        m3.setOnAction(event -> WindowUtil.create(
                new EvaluationTabEdition<>(manager, modules.getSelectionModel().getSelectedItem(), Subject.class),
                getWindow()
        ));
        menu.getStyleClass().addAll("dark-bg", "text-white");
        menu.getItems().forEach(m -> m.getStyleClass().addAll("dark-bg", "text-white"));

        return menu;
    }

}
