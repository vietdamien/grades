/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : SubjectListCell
 * Author      : Damien Nguyen
 * Date        : Sunday, June 28 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.controllers.cells;

import com.vietdamien.grades.business.Subject;
import com.vietdamien.grades.data.DataManager;
import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;

public class SubjectListCell extends EvaluationListCell<Subject> {

    private static final double LR_PADDING = 13;

    public SubjectListCell(DataManager manager) {
        super(manager);

        content.setPadding(new Insets(0, LR_PADDING, 0, LR_PADDING));
    }

    @Override
    protected void updateItem(Subject item, boolean empty) {
        super.updateItem(item, empty);

        if (!isEmpty()) {
            value.textProperty().bind(Bindings.format("%05.2f", item.averageProperty()));
        }
    }

}
