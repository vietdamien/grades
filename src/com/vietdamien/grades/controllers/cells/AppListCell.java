/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : GradesListCell
 * Author      : Damien Nguyen
 * Date        : Sunday, June 28 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.controllers.cells;

import com.vietdamien.grades.data.DataManager;
import javafx.scene.control.ListCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class AppListCell<T> extends ListCell<T> {

    protected final DataManager manager;

    protected VBox       box;
    protected BorderPane content;

    public AppListCell(DataManager manager) {
        this.manager = manager;
        this.box     = new VBox();
        this.content = new BorderPane();

        content.getStyleClass().setAll("transparent");
        box.getChildren().addAll(content);
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            textProperty().unbind();
            setText(null);
            setGraphic(null);
        } else {
            setGraphic(box);
        }
    }

}
