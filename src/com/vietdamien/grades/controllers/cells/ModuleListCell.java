/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : ModuleListCell
 * Author      : Damien Nguyen
 * Date        : Monday, June 29 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.controllers.cells;

import com.vietdamien.grades.business.Module;
import com.vietdamien.grades.business.Subject;
import com.vietdamien.grades.controllers.DoubleSidedPane;
import com.vietdamien.grades.controllers.EvaluationTabEdition;
import com.vietdamien.grades.data.DataManager;
import com.vietdamien.grades.util.view.I18n;
import com.vietdamien.grades.util.view.WindowUtil;
import javafx.beans.binding.Bindings;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class ModuleListCell extends EvaluationListCell<Module> {

    private final ListView<Subject> subjects;

    public ModuleListCell(DataManager manager, DoubleSidedPane controller) {
        super(manager);

        this.getStyleClass().addAll("transparent-list-cell");

        label.getStyleClass().addAll("heading");
        value.getStyleClass().addAll("heading");
        coefficient.getStyleClass().addAll("heading");

        subjects = new ListView<>();
        subjects.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
            Subject subject = subjects.getSelectionModel().getSelectedItem();
            if (subject != null && event.getButton() == MouseButton.PRIMARY) {
                controller.flip(subject);
            }
        });

        box.getChildren().add(subjects);
    }

    @Override
    protected void updateItem(Module item, boolean empty) {
        super.updateItem(item, empty);

        if (!isEmpty()) {
            value.textProperty().bind(Bindings.format("%05.2f", item.averageProperty()));
            subjects.itemsProperty().bind(item.componentsProperty());
            subjects.setCellFactory(subjectListView -> new SubjectListCell(manager));
            subjects.prefHeightProperty().bind(Bindings.size(subjects.itemsProperty().get()).multiply(30));

            if (subjects.getContextMenu() == null) {
                subjects.setContextMenu(buildSubjectContextMenu(item));
            }
        }
    }

    private ContextMenu buildSubjectContextMenu(Module module) {
        var m1   = new MenuItem(I18n.getString("Edit_subject"));
        var m2   = new MenuItem(I18n.getString("Remove_subject"));
        var menu = new ContextMenu(m1, m2);

        m1.setOnAction(event -> WindowUtil.create(
                new EvaluationTabEdition<>(manager, module, subjects.getSelectionModel().getSelectedItem())
        ));
        m2.setOnAction(event -> module.remove(subjects.getSelectionModel().getSelectedItem()));
        menu.getStyleClass().addAll("dark-bg");
        menu.getItems().forEach(m -> m.getStyleClass().addAll("dark-bg", "text-white"));

        return menu;
    }

}
