/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : EvaluationListCell
 * Author      : Damien Nguyen
 * Date        : Sunday, June 28 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.controllers.cells;

import com.vietdamien.grades.business.Evaluation;
import com.vietdamien.grades.data.DataManager;
import javafx.beans.binding.Bindings;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class EvaluationListCell<T extends Evaluation> extends AppListCell<T> {

    protected HBox hBox;

    protected Label label;
    protected Label value;
    protected Label coefficient;

    public EvaluationListCell(DataManager manager) {
        super(manager);

        this.hBox        = new HBox();
        this.label       = new Label();
        this.value       = new Label();
        this.coefficient = new Label();

        hBox.getChildren().addAll(this.coefficient, this.label);
        hBox.setSpacing(15);

        content.setLeft(hBox);
        content.setRight(value);
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);

        if (!isEmpty()) {
            label.textProperty().bind(item.labelProperty());
            coefficient.textProperty().bind(Bindings.format("%05.2f", item.coefficientProperty()));
        } else {
            label.textProperty().unbind();
            value.textProperty().unbind();
            coefficient.textProperty().unbind();
        }
    }

}
