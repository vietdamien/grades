/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : EvaluationTabEdition
 * Author      : Damien Nguyen
 * Date        : Tuesday, July 21 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.controllers;

import com.vietdamien.grades.business.Evaluable;
import com.vietdamien.grades.business.EvaluationTab;
import com.vietdamien.grades.data.DataManager;
import com.vietdamien.grades.util.view.WindowUtil;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.util.converter.NumberStringConverter;

public class EvaluationTabEdition<P extends Evaluable<T>, T extends EvaluationTab<?>> extends Edition {

    protected P parent;
    protected T oldTab;
    protected T tab;

    @FXML
    private TextField label;
    @FXML
    private TextField coefficient;

    @FXML
    private VBox buttons;

    @SuppressWarnings("unchecked")
    public EvaluationTabEdition(DataManager manager, P parent, T tab) {
        super(manager);

        if (!tab.isEmpty()) {
            try {
                this.oldTab = ((Class<T>) tab.getClass()).getDeclaredConstructor().newInstance();
                oldTab.setLabel(tab.getLabel());
                oldTab.setCoefficient(tab.getCoefficient());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        this.tab    = tab;
        this.parent = parent;
    }

    public EvaluationTabEdition(DataManager manager, P parent, Class<T> c) {
        super(manager);

        try {
            this.tab    = c.getDeclaredConstructor().newInstance();
            this.parent = parent;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void initialize() {
        var converter = new NumberStringConverter();

        label.textProperty().bindBidirectional(tab.labelProperty());
        Bindings.bindBidirectional(coefficient.textProperty(), tab.coefficientProperty(), converter);

        if (oldTab != null) {
            buildRemoveButton(event -> {
                parent.remove(tab);
                WindowUtil.close(getWindow());
            });
        }
    }

    @Override
    protected void cancel() {
        if (oldTab != null) {
            tab.setCoefficient(oldTab.getCoefficient());
            tab.setLabel(oldTab.getLabel());
        }

        super.cancel();
    }

    @Override
    protected void submit() {
        if (!tab.isEmpty() && parent != null && oldTab == null) {
            parent.add(tab);
        }

        super.submit();
    }

    @Override
    protected void close() {
        label.textProperty().unbindBidirectional(tab.labelProperty());
        coefficient.textProperty().unbindBidirectional(tab.coefficientProperty());

        super.close();
    }

}
