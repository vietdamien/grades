package com.vietdamien.grades.data.persistence;/*
 *********************************************************************************************************************
 * Project name: Grades
 * File name   : DataSaver
 * Author      : Damien Nguyen
 * Date        : Friday, July 03 2020
 * ********************************************************************************************************************/

import com.vietdamien.grades.business.Student;

public interface DataSaver {

    void save(Student student);

}
