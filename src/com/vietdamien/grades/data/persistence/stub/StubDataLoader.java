/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : StubDataLoader
 * Author      : Damien Nguyen
 * Date        : Friday, July 03 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.data.persistence.stub;

import com.vietdamien.grades.business.Module;
import com.vietdamien.grades.business.Student;
import com.vietdamien.grades.business.Subject;
import com.vietdamien.grades.data.persistence.DataLoader;

public class StubDataLoader implements DataLoader {

    @Override
    public Student load() {
        var s = new Student("NGUYEN", "Damien", "damien.nguyen@etu.uca.fr");

        var m1 = new Module(8, "Aide à la décision et mathématiques appliquées");
        var m2 = new Module(9, "Informatique");
        var m3 = new Module(5, "Sciences de l'ingénieur");
        var m4 = new Module(5, "Sciences humaines et sociales");

        var s11 = new Subject(1, "Allemand");
        var s12 = new Subject(1, "Anglais");
        var s13 = new Subject(1, "Expression - Communication");
        m4.add(s11);
        m4.add(s12);
        m4.add(s13);
        // s11.add(new Grade(1, 14, "Vocabulaire"));
        // s11.add(new Grade(2, 14, "Compréhension orale"));
        // s12.add(new Grade(2, 18, "Essay"));
        // s12.add(new Grade(1, 18.5f, "Presentation"));
        // s12.add(new Grade(.5f, 17, "Vocabulary"));
        // s13.add(new Grade(1, 15, "Cours"));
        // s13.add(new Grade(1, 16, "Exposé"));
        // s13.add(new Grade(1, 17, "Article de presse"));

        var s21 = new Subject(1.5f, "Algorithmique et structures de données");
        var s22 = new Subject(1, "Bases de données");
        var s23 = new Subject(1, "Sensibilisation à la cyber-sécurité");
        var s24 = new Subject(1, "Systèmes d'exploitation");
        m2.add(s21);
        m2.add(s22);
        m2.add(s23);
        m2.add(s24);

        var s31 = new Subject(1, "Automatique");
        var s32 = new Subject(.5f, "TP - Physique");
        var s33 = new Subject(.5f, "TP - Transmission de données");
        m3.add(s31);
        m3.add(s32);
        m3.add(s33);

        var s41 = new Subject(.5f, "Analyse de données");
        var s42 = new Subject(1, "Analyse numérique");
        var s43 = new Subject(1, "Programmation linéaire");
        var s44 = new Subject(1, "Probabilités");
        m1.add(s41);
        m1.add(s42);
        m1.add(s43);
        m1.add(s44);

        s.add(m4);
        s.add(m2);
        s.add(m3);
        s.add(m1);

        return s;
    }

}
