/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : XMLDataSaver
 * Author      : Damien Nguyen
 * Date        : Friday, July 03 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.data.persistence.xml;

import com.vietdamien.grades.business.Student;
import com.vietdamien.grades.data.persistence.DataSaver;
import com.vietdamien.grades.data.persistence.Paths;

import java.beans.XMLEncoder;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class XMLDataSaver implements DataSaver {

    @Override
    public void save(Student student) {
        try (XMLEncoder encoder = new XMLEncoder(new FileOutputStream(Paths.buildBackupFile()))) {
            encoder.writeObject(student);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
