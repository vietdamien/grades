/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : XMLDataLoader
 * Author      : Damien Nguyen
 * Date        : Friday, July 03 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.data.persistence.xml;

import com.vietdamien.grades.business.Student;
import com.vietdamien.grades.data.persistence.DataLoader;
import com.vietdamien.grades.data.persistence.Paths;

import java.beans.XMLDecoder;
import java.io.FileInputStream;
import java.io.IOException;

public class XMLDataLoader implements DataLoader {

    @Override
    public Student load() {
        Student student = null;

        try (XMLDecoder decoder = new XMLDecoder(new FileInputStream(Paths.buildBackupFile()))) {
            student = (Student) decoder.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return student;
    }

}
