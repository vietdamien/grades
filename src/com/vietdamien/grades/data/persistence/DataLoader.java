package com.vietdamien.grades.data.persistence;/*
 *********************************************************************************************************************
 * Project name: Grades
 * File name   : DataLoader
 * Author      : Damien Nguyen
 * Date        : Friday, June 26 2020
 * ********************************************************************************************************************/

import com.vietdamien.grades.business.Student;

public interface DataLoader {

    Student load();

}
