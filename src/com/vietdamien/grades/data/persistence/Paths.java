/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : Paths
 * Author      : Damien Nguyen
 * Date        : Friday, July 03 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.data.persistence;

public class Paths {

    public static final String BASE_DIR      = "bak/";
    public static final String BASE_FILE     = "backup";
    public static final String XML_EXTENSION = ".xml";

    private Paths() {
    }

    public static String buildBackupFile() {
        return BASE_DIR + BASE_FILE + XML_EXTENSION;
    }

}
