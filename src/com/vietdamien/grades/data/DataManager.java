/* *********************************************************************************************************************
 * Project name: Grades
 * File name   : DataManager
 * Author      : Damien Nguyen
 * Date        : Saturday, June 27 2020
 * ********************************************************************************************************************/

package com.vietdamien.grades.data;

import com.vietdamien.grades.business.Student;
import com.vietdamien.grades.data.persistence.DataLoader;
import com.vietdamien.grades.data.persistence.DataSaver;
import com.vietdamien.grades.data.persistence.Paths;
import com.vietdamien.grades.data.persistence.xml.XMLDataLoader;
import com.vietdamien.grades.data.persistence.xml.XMLDataSaver;

import java.io.File;

public class DataManager {

    private final DataLoader loader;
    private final DataSaver  saver;

    protected Student student;

    public DataManager() {
        this.loader = new File(Paths.buildBackupFile()).exists() ? new XMLDataLoader() : null;
        this.saver  = new XMLDataSaver();
        load();
    }

    public Student getStudent() {
        return student;
    }

    public void load() {
        student = loader == null ? new Student() : loader.load();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void save() {
        File directory = new File(Paths.BASE_DIR);

        if (!directory.isDirectory()) {
            directory.delete();
        }

        if (!directory.exists()) {
            directory.mkdir();
        }

        saver.save(student);
    }

}
