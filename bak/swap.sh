#!/bin/bash

sum=0
for file in $(ls); do
    if [[ "$file" = "s1.xml" ]]; then
        sum=$(($sum+1))
    fi
done

if [ $sum -gt 0 ]; then
    mv backup.xml s2.xml
    mv s1.xml backup.xml
else
    mv backup.xml s1.xml
    mv s2.xml backup.xml
fi

